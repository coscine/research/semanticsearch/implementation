This repository contains the code created in the master thesis ["An Efficient Semantic Search Engine for Research Data in an RDF-based Knowledge Graph"](http://dx.doi.org/10.18154/RWTH-2020-09883) to implement a search engine for RDF-based metadata graphs based on Virtuoso, Elasticsearch and the developed mapping of the data.

## Code documentation:

The code documentation can be found under *./docs/index.html*.

## Setup/Prerequisites:

- Virtuoso
    - Virtuoso.ini file 
        - NumberOfBuffers >= 660000
        - MaxDirtyBuffers >= 495000
    - Define the following graphs as rulesets named "ruleset" for inference rules with `rdfs_rule_set ('<rulesetname>', '<graphname>') ;`
        - http://www.dfg.de/dfg_profil/gremien/fachkollegien/faecher/
        - http://www.w3.org/ns/org#
        - http://xmlns.com/foaf/0.1/
- Data mentioned in ["Sample Dataset for Search Engine Evaluation for Research Data in an RDF-based Knowledge Graph"](http://dx.doi.org/10.18154/RWTH-2020-09886)
    - All named_graphs, use virtuoso.db/graphs.db
- Elasticsearch
- Specified packages

## How to use:

1. (Re-)Build solution (Install/restore necessary packages)
2. Start Virtuoso
3. Start Elasticsearch
4. Run *SemanticSearchImplementation.exe* with the necessary arguments


### Commandline arguments

| Option        | Description | 
| ------------- |-------------| 
|-a, --action   | Required. Possible action: search, reindex, index, delete or add|
|-v             | (Default: localhost) Server name of Virtuoso|
|--vp           | (Default: 1111) Port of Virtuoso|
|--db           | (Default: db) DB of Virtuoso|
|--dbUser       | (Default: dba) Database user of Virtuoso|
|--dbPassword   | (Default: dba) Database password of Virtuoso|
|-e             | (Default: localhost) Server name of Elasticsearch|
|--ep           | (Default: 9200) Port of Elasticsearch|
|-l             | (Default: en) Specify language|
|-d, --doc      | ID of metadata graph|
|-q, --query    | (Default: *) Elasticsearch query|
|--adv          | (Default: false) Set true for advanced Elasticsearch search syntax|
|-u, --user     | (Default: ) Specify user or only public metadata records could be found|