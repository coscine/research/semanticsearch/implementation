﻿using NUnit.Framework;

namespace SemanticSearchImplementation.Tests
{
    [TestFixture]
    public class RdfClientTests
    {
        IRdfConnector _connector;
        RdfClient _rdfClient;

        [OneTimeSetUp]
        public void Init()
        {
            _connector = new VirtuosoRdfConnector();
            _rdfClient = new RdfClient(_connector, "en");

            // TODO: automatic creation of the desired database state(containing defined named graphs)
            // TODO: maybe use an additional test database using docker (https://hub.docker.com/r/tenforce/virtuoso/)
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            // TODO: cleaning database to recreate the original state
        }

        [Test]
        public void GetCurrentIndexTest()
        {
            var index = _rdfClient.GetCurrentIndexVersion();
            Assert.AreEqual(1, index);
        }

        // TODO: add further tests and test classes
    }
}