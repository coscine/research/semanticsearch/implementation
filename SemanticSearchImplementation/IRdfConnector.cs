﻿using VDS.RDF;
using VDS.RDF.Query;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Provides all necessary functions for querying and manipulating the RDF-based knowledge graph.
    /// </summary>
    public interface IRdfConnector { 

        /// <summary>
        /// Returns a metadata graph.
        /// </summary>
        /// <param name="graphName">ID of the metadata graph.</param>
        /// <returns>A graph.</returns>
        Graph GetGraph(string graphName);

        /// <summary>
        /// Queries the knowledge graph with a parametrized string and a <c>Graph</c> as result.
        /// </summary>
        /// <param name="query">Parametrized SPARQL query which returns a <c>Graph</c>.</param>
        /// <returns>A <c>Graph</c>.</returns>
        IGraph QueryWithResultGraph(SparqlParameterizedString query);

        /// <summary>
        /// Queries the knowledge graph with a query and possibly with stored inference rules.
        /// </summary>
        /// <param name="query">String representation of a SPARQL query.</param>
        /// <param name="withInference">Flag which indicates the use of the inference rules.</param>
        /// <returns>A <c>SparqlResultSet</c>.</returns>
        SparqlResultSet QueryWithResultSet(string query, bool withInference = true);

        /// <summary>
        /// Queries the knowledge graph with a parametrized string and a <c>SparqlResultSet</c> as result.
        /// </summary>
        /// <param name="query">Parametrized SPARQL query which returns a <c>SparqlResultSet</c>.</param>
        /// <returns>A <c>SparqlResultSet</c>.</returns>
        SparqlResultSet QueryWithResultSet(SparqlParameterizedString query);

        /// <summary>
        /// Updates the knoeldge graph with a parametrized string.
        /// </summary>
        /// <param name="query">Parametrized SPARQL query.</param>
        void Update(SparqlParameterizedString query);
    }
}