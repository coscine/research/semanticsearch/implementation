﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using VDS.RDF;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Represents a specific application profile.
    /// </summary>
    public class SpecificApplicationProfile : ApplicationProfile
    {
        private readonly string _applicationProfileId;
        private readonly IDictionary<string, LiteralRule> _specificLiteralRules;
        private readonly IEnumerable<AdditionalRule> _specificAdditionalRules;

        public SpecificApplicationProfile(RdfClient RDFClient, string applicationProfileId) : base(RDFClient) {
            _applicationProfileId = applicationProfileId;
            _specificLiteralRules = _RDFClient.ConstructLiteralRules(_applicationProfileId);
            _specificAdditionalRules = _RDFClient.ConstructAdditionalRules(_applicationProfileId);
        }

        /// <summary>
        /// Executes specific additional rules for a given metadata graph. 
        /// </summary>
        /// remarks>Depending on the rule, triples can also be created for other metadata graphs.</remarks>
        /// <param name="graphName">ID of metadata graph</param>
        /// <returns>A dictionary which contains a list of created triples (value) for graphs (key).</returns>
        public IDictionary<string, List<Triple>> GetAdditionalTriples(string graphName)
        {
            IDictionary<string, List<Triple>> triples = new Dictionary<string, List<Triple>>();
            foreach (var additionalRule in _specificAdditionalRules)
            {
                var constructedTriples = additionalRule.ExecuteRule(graphName);

                foreach (var graph in constructedTriples.Keys)
                {
                    if (!triples.ContainsKey(graph))
                    {
                        triples.Add(graph, new List<Triple>());
                    }
                    triples[graph].AddRange(constructedTriples[graph]);
                }                
            }
            return triples;
        }

        /// <summary>
        /// Creates JSON object for a given label (field) and instance (value) 
        /// by applying the corresponding literal rules.
        /// </summary>
        /// <param name="label">The label of the field.</param>
        /// <param name="instance">An instance of a specific class for which a literal rule needs to be applied.</param>
        /// <returns>A JSON object containing the created field-value pair.</returns>
        public JObject GetLiterals(string label, string instance)
        {
            // TODO: what about equivalent classes?
            var classes = _RDFClient.GetDirectClasses(instance);
            var constructRules = GetAllConstructRulesForLiterals(classes);
            List<string> literals = new List<string>();
            foreach (var constructRule in constructRules)
            {                
                literals.AddRange(constructRule.ExecuteRule(instance));
            }
            // if no literal could be found
            if (literals.Count == 0)
            {
                var literal = _RDFClient.GuessLabel(instance);
                if (String.IsNullOrEmpty(literal))
                {
                    throw new NotIndexableException("No literal could be generated for object.");
                }
                else
                {
                    literals.Add(literal);
                }
            }

            return new JObject()
            {
                { label, CreateJTokenForESFromList(literals.Distinct())}
            };
        }

        /// <summary>
        /// Creates a JToken of a list of literals.
        /// </summary>
        /// <param name="list">A list of literals.</param>
        /// <returns>A JToken which is either a single string or an array.</returns>
        private JToken CreateJTokenForESFromList(IEnumerable<string> list)
        {
            if (list.Count() == 1)
            {
                return list.First();
            }
            else
            {
                return new JArray
                {
                    list
                };
            }
        }

        /// <summary>
        /// Collects all relevant literal rules for the given classes.
        /// </summary>
        /// <remarks>Rules of direct classes are used. Rules for parent classes are only considered if
        /// no other rules could be found. A specific literal rule overwrites a generl literal rule for the same class.</remarks>
        /// <param name="classes">A list of classes for which literal rules are searched.</param>
        /// <returns>An enumerator of corresponding literal rules.</returns>
        private IEnumerable<LiteralRule> GetAllConstructRulesForLiterals(IEnumerable<string> classes)
        {
            // only use rules of all direct classes, if there are no: use rules of parents until root class is reached
            IList<LiteralRule> rules = new List<LiteralRule>();
            foreach (var classElement in classes)
            {
                // check for specific rule in application profile
                if (_specificLiteralRules.ContainsKey(classElement))
                {                    
                    rules.Add(_specificLiteralRules[classElement]);
                }
                else
                {
                    // only use general rule for this class if no specific rule for this class is defined in applicationprofile
                    if (_generalLiteralRules.ContainsKey(classElement))
                    {
                        rules.Add(_generalLiteralRules[classElement]);
                    }
                }
            }
            if (rules.Count > 0)
            {
                return rules;
            }
            else
            {
                // if no rules could be found, search for rules of parent classes
                var parentClasses = _RDFClient.GetParentClasses(classes);
                if (parentClasses.Count == 0)
                {
                    // use root class 
                    parentClasses.Add(Uris.COSCINE_SEARCH_ROOT_CLASS);
                }
                return GetAllConstructRulesForLiterals(parentClasses);
            }
        }
    }
}