﻿using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Contains methods to parse the objects of a metadata graph according to the mapping type.
    /// </summary>
    public class DataTypeParser
    {
        private RdfClient _RDFClient;

        public DataTypeParser(RdfClient RDFClient)
        {
            _RDFClient = RDFClient;
        }

        /// <summary>
        /// Parses the node given the mapping and profile.
        /// </summary>
        /// <param name="label">The label of the field.</param>
        /// <param name="node">The node which needs to be converted into a (list of) literal.</param>
        /// <param name="indexMapper">The <c>ElasticsearchIndexMapper</c>.</param>
        /// <param name="profile">The specific applicationprofile the node belongs to.</param>
        /// <returns>A JSON object containing the label (key) and the corresponding literal/list of literals (value).</returns>
        public JObject Parse(string label, INode node, ElasticsearchIndexMapper indexMapper, SpecificApplicationProfile profile)
        {
            if (node.NodeType == NodeType.Literal)
            {
                // in the application profile a data type was specified

                ILiteralNode literalNode = (ILiteralNode)node;
                var type = indexMapper.GetTypeOfProperty(label);

                // type of properties of additional triples do not appear in profile
                // check mapping if field already exists or generate type from type of object node
                if (String.IsNullOrEmpty(type))
                {
                    var literalType = XmlSpecsHelper.XmlSchemaDataTypeString;
                    if (literalNode.DataType != null)
                    {
                        literalType = literalNode.DataType.ToString();
                    }
                    type = indexMapper.GetSearchType(_RDFClient.GetDataType(literalType));
                }

                return ParseLiteralNode(label, type, literalNode);

            }
            else if (node.NodeType == NodeType.Uri)
            {
                // in the application profile a class was specified

                return profile.GetLiterals(label, node.ToString());
            }
            else
            {
                throw new NotIndexableException("Object has unknown type.");
            }
        }

        /// <summary>
        /// Parses literal nodes depending on the needed Elasticsearch type specified in the mapping for a label.
        /// </summary>
        /// <param name="label">The label of the property which is used as field.</param>
        /// <param name="type">The type specified in the Elasticsearch mapping for the label.</param>
        /// <param name="literalNode">The literal node which needs to be parsed.</param>
        /// <returns>A JSON object containing the label (key) and the corresponding literal (value).</returns>
        private JObject ParseLiteralNode(string label, string type, ILiteralNode literalNode)
        {
            switch (type)
            {
                case ElasticsearchIndexMapper.TEXT:
                    return ParseString(label, literalNode);
                case ElasticsearchIndexMapper.KEYWORD:
                    return ParseString(label, literalNode);
                case ElasticsearchIndexMapper.BOOLEAN:
                    return ParseBoolean(label, literalNode);
                case ElasticsearchIndexMapper.INTEGER:
                    return ParseInt(label, literalNode);
                case ElasticsearchIndexMapper.DATE:
                    return ParseDate(label, literalNode);
                default:
                    throw new NotIndexableException("Unknown property type");
            }
        }

        /// <summary>
        /// Parses a literal node into a boolean and adds second field for a written representation of the label and boolean value.
        /// </summary>
        /// <param name="label">The label of the property which is used as field.</param>
        /// <param name="literalNode">The literal node which needs to be parsed.</param>
        /// <returns>A JSON object containing the label (key) and the corresponding boolean (value) as well as a written variant.</returns>
        private JObject ParseBoolean(string label, ILiteralNode literalNode)
        {
            var val = literalNode.Value;
            bool boolValue = false;
            if (String.Equals(val, "true") || String.Equals(val, "1"))
            {
                boolValue = true;
            }
            return new JObject()
            {
                { label, boolValue },
                { label + ElasticsearchIndexMapper.BOOLEAN_EXTENSION, label.Replace("_", " ") + " " + boolValue.ToString().ToLower() }
            };
        }

        /// <summary>
        /// Parses a literal node into a date and adds fields for day, month and year.
        /// </summary>
        /// <param name="label">The label of the property which is used as field.</param>
        /// <param name="literalNode">The literal node which needs to be parsed.</param>
        /// <returns>A JSON object containing the label (key) and the corresponding date (value) as well as pairs for day, year and month.</returns>
        public JObject ParseDate(string label, ILiteralNode literalNode)
        {
            var dateTime = Convert.ToDateTime(literalNode.Value);

            return new JObject() {
                { label, literalNode.Value },
                // additional date fields
                { label + ElasticsearchIndexMapper.YEAR_EXTENSION, dateTime.Year },
                { label + ElasticsearchIndexMapper.MONTH_EXTENSION, dateTime.ToString("MMMM", new CultureInfo(_RDFClient.GetLanguage())) },
                // TODO: remove dayExtension field
                { label + ElasticsearchIndexMapper.DAY_EXTENSION, dateTime.Day }
            };
        }

        /// <summary>
        /// Parses a literal node into a string.
        /// </summary>
        /// <param name="label">The label of the property which is used as field.</param>
        /// <param name="literalNode">The literal node which needs to be parsed.</param>
        /// <returns>A JSON object containing the label (key) and the corresponding string (value).</returns>
        public JObject ParseString(string label, ILiteralNode literalNode)
        {
            return new JObject() {
                { label, literalNode.Value }
            };
        }

        /// <summary>
        /// Parses a literal node into an integer.
        /// </summary>
        /// <param name="label">The label of the property which is used as field.</param>
        /// <param name="literalNode">The literal node which needs to be parsed.</param>
        /// <returns>A JSON object containing the label (key) and the corresponding integer (value).</returns>
        public JObject ParseInt(string label, ILiteralNode literalNode)
        {
            return new JObject()
            {
                { label, Convert.ToInt32(literalNode.Value) }
            };
        }
    }
}