﻿using CommandLine;
using System;
using System.Threading.Tasks;

namespace SemanticSearchImplementation
{

    /// <summary>
    /// Entry class which interprets the command line arguments and calls corresponding functions.
    /// </summary>
    public class SemanticSearch
    {
        // provided methods
        private const string SEARCH = "search";
        private const string REINDEX = "reindex";
        private const string INDEX = "index";
        private const string DELETE = "delete";
        private const string ADD = "add";

        /// <summary>
        /// Class to define valid options, and to receive the parsed options.
        /// </summary>
        public class Options
        {
            // required parameter
            [Option('a', "action", Required = true, HelpText = "Possible action: search, reindex, index, delete or add")]
            public string Action { get; set; }

            // optional parameter
            [Option('v', Default = "localhost", HelpText = "Server name of Virtuoso")]
            public string VirtuosoServer { get; set; }

            [Option("vp", Default = 1111, HelpText = "Port of Virtuoso")]
            public int VirtuosoPort{ get; set; }

            [Option("db", Default = "db", HelpText = "DB of Virtuoso")]
            public string VirtuosoDb { get; set; }

            [Option("dbUser", Default = "dba",  HelpText = "Database user of Virtuoso")]
            public string VirtuosoDbUser { get; set; }

            [Option( "dbPassword", Default = "dba", HelpText = "Database password of Virtuoso")]
            public string VirtuosoDbPassword { get; set; }

            [Option('e', Default = "localhost", HelpText = "Server name of Elasticsearch")]
            public string ElasticsearchServer { get; set; }

            [Option("ep", Default = 9200, HelpText = "Port of Elasticsearch")]
            public int ElasticsearchPort { get; set; }

            [Option('l', Default = "en", HelpText = "Specify language")]
            public string Language { get; set; }

            // dependent on action

            // add/update/delete
            [Option('d', "doc", HelpText = "ID of metadata graph")]
            public string Document { get; set; }

            // search 
            [Option('q', "query", Default="*", HelpText = "Elasticsearch query")]
            public string Query { get; set; }

            [Option("adv", Default = false, HelpText = "Set true for advanced Elasticsearch search syntax")]
            public bool Advanced { get; set; }

            [Option('u', "user", Default = "", HelpText = "Specify user or only public metadata records could be found")]
            public String User { get; set; }
        }

        public static void Main(string[] args)
        {
            _ = Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o =>
                {
                    IRdfConnector connector = new VirtuosoRdfConnector(o.VirtuosoServer, o.VirtuosoPort, o.VirtuosoDb, o.VirtuosoDbUser, o.VirtuosoDbPassword);
                    ISearchClient searchClient = new ElasticsearchSearchClient(o.ElasticsearchServer, "" + o.ElasticsearchPort);
                    RdfSearchMapper mapper = new RdfSearchMapper(connector, searchClient, o.Language);

                    // execute method specified in command line arguments
                    Task task = Task.CompletedTask;
                    switch (o.Action)
                    {
                        case SEARCH:
                            task = mapper.SearchAsync(o.Query, o.User, o.Advanced);
                            break;
                        case REINDEX:
                            task = mapper.ReIndexAsync();
                            break;
                        case INDEX:
                            task = mapper.CreateIndexAsync();
                            break;
                        case ADD:
                            if (CheckDocumentIsSet(o.Document))
                            {
                                task = mapper.AddDocumentAsync(o.Document);
                            }
                            break;
                        case DELETE:
                            if (CheckDocumentIsSet(o.Document))
                            {
                                task = mapper.DeleteDocumentAsync(o.Document);
                            }
                            break;
                        default:
                            Console.WriteLine($"Action parameter does not exist");
                            break;
                    }
                    task.Wait();
                });
        }

        /// <summary>
        /// Checks if a document is set.
        /// </summary>
        /// <param name="doc">ID of the metadata graph.</param>
        /// <returns></returns>
        private static bool CheckDocumentIsSet(string doc)
        {
            if (String.IsNullOrEmpty(doc))
            {
                Console.WriteLine("A document needs to be specified");
                return false;
            } else
            {
                return true;
            }
        }
    }
}