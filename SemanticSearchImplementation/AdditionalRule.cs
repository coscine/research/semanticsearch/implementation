﻿using System;
using System.Collections.Generic;
using System.Linq;
using VDS.RDF;
using VDS.RDF.Query;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Represents an additional rule.
    /// </summary>
    public class AdditionalRule
    {
        private readonly IRdfConnector _connector;
        private SparqlParameterizedString _queryString;

        public AdditionalRule(IRdfConnector connector, SparqlParameterizedString queryString)
        {
            _connector = connector;
            _queryString = queryString;
        }

        /// <summary>
        /// Executes an additional rule for a specific metadata graph.
        /// </summary>
        /// <remarks>Additional rules can affect the maping of other existing metadata graphs.</remarks>
        /// <param name="graphName">ID of metadata graph.</param>
        /// <returns>A dictionary containing the IDs of the metadata graphs (key) and the corresponding constructed triples (value).</returns>
        public IDictionary<string, List<Triple>> ExecuteRule(string graphName)
        {
            _queryString.SetUri(RdfClient.LABEL_ADDITIONAL_RULE, new Uri(graphName));

            var constructedTriples = _connector.QueryWithResultGraph(_queryString);

            // group by subject to distinguish different metadata records
            var groupedTriples = constructedTriples.Triples.GroupBy(x => x.Subject.ToString());

            return groupedTriples.ToDictionary(x => x.Key, x => x.ToList());
        }
    }
}