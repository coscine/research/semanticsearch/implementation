﻿using System;
using System.Collections.Generic;
using System.Linq;
using VDS.RDF;
using VDS.RDF.Query;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Represents a literal rule.
    /// </summary>
    public class LiteralRule
    {
        private readonly IRdfConnector _connector;
        private readonly SparqlParameterizedString _queryString;
        private readonly string _language;

        public LiteralRule(IRdfConnector connector, SparqlParameterizedString queryString, string language)
        {
            _connector = connector;
            _queryString = queryString;
            _language = language;
        }

        /// <summary>
        /// Executes a literal rule for a given instance of a class.
        /// </summary>
        /// <param name="instance">A URI of a concrete instance of a class.</param>
        /// <returns>A list of constructed literals.</returns>
        public IEnumerable<string> ExecuteRule(string instance)
        {
            _queryString.SetUri(RdfClient.LABEL_LITERAL_RULE, new Uri(instance));
            var result = _connector.QueryWithResultGraph(_queryString);
            return result.Triples.Where(x =>
            {
                var language = ((ILiteralNode)x.Object).Language;
                return String.Equals(language, _language) || String.Equals(language, String.Empty);
            }).Select(x => ((ILiteralNode)x.Object).Value).Distinct();
        }
    }
}