﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Provides all necessary functions to implement a document-based search engine.
    /// </summary>
    public interface ISearchClient
    {
        /// <summary>
        /// Queries the mappings of the current index.
        /// </summary>
        /// <returns>A dictionary containing the fields and corresponding types.</returns>
        Task<IDictionary<string, string>> GetMappingAsync();

        /// <summary>
        /// Creates an index with the given settings and mappings.
        /// </summary>
        /// <param name="content">JSON object containing the settings and mappings.</param>
        /// <param name="index">The index name.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        Task CreateIndexAsync(JObject content, string index);

        /// <summary>
        /// Adds the given documents as a bulk upload.
        /// </summary>
        /// <param name="documents">An enumerator of all documents as JSON object.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        Task AddDocumentsAsync(IEnumerable<JObject> documents);

        /// <summary>
        /// Changes the alias from the old to the new index.
        /// </summary>
        /// <param name="from">Name of old index.</param>
        /// <param name="to">Name of new index.</param>
        /// <returns></returns>
        Task SwitchAliasAsync(string from, string to);

        /// <summary>
        /// Deletes the given index.
        /// </summary>
        /// <param name="index">Name of the index.</param>
        /// <returns>A task that represents the asynchronous delete operation.</returns>
        Task DeleteIndexAsync(string index);

        /// <summary>
        /// Adds/updates a new document and possibly changes existing documents.
        /// </summary>
        /// <remarks>Additional rules can influence the mapping of existing metadata graphs.</remarks>
        /// <param name="graphName">ID of the metadata graph to be added/updated.</param>
        /// <param name="documents">A dictionary containing the ID of metadata graphs (key)
        /// and corresponding JSON objects (value).</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        Task AddDocumentAsync(string graphName, IDictionary<string, JObject> documents);

        /// <summary>
        /// Deletes a document and possibly changes other existing documents.
        /// </summary>
        /// <remarks>Additional rules can influence the mapping of existing metadata graphs.</remarks>
        /// <param name="graphName">ID of the metadata graph to be deleted.</param>
        /// <param name="documents">A dictionary containing the ID of metadata graphs (key)
        /// and corresponding JSON objects (value).</param>
        /// <returns>A task that represents the asynchronous delete operation.</returns>
        Task DeleteDocumentAsync(string graphName, IDictionary<string, JObject> documents);

        /// <summary>
        /// Updates the current index.
        /// </summary>
        /// <param name="index">Name of new current index.</param>
        void ChangeIndex(string index);

        /// <summary>
        /// Searches the index using the alias.
        /// </summary>
        /// <param name="query">The search query of the user.</param>
        /// <param name="projects">List of allowed projects (of a user).</param>
        /// <param name="advanced">Flag to specify simple or advanced search syntax.</param>
        /// <param name="size">Number of results.</param>
        /// <param name="from">Position from which the results should be returned.</param>
        /// <param name="sorting">Sorting of the results <see>
        /// (see https://www.elastic.co/guide/en/elasticsearch/reference/current/sort-search-results.html). </see></param>
        /// <returns>The task result contains a dictionary containing the IDs of the found metadata graphs (key) 
        /// and the corresponding ranking (value).</returns>
        Task<IDictionary<string, double>> SearchAsync(string query, IEnumerable<string> projects, bool advanced , int size, int from, string sorting);
    }
}