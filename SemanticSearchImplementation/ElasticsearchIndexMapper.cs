﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using static SemanticSearchImplementation.RdfClient;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Contains methods to create, change and handle the current Elasticsearch index.
    /// </summary>
    public class ElasticsearchIndexMapper
    {
        // ES date types
        public const string TEXT = "text";
        public const string KEYWORD = "keyword";
        public const string BOOLEAN = "boolean";
        public const string INTEGER = "integer";
        public const string DATE = "date";

        // special fields
        public const string YEAR_EXTENSION = "_year";
        public const string MONTH_EXTENSION = "_month";
        public const string DAY_EXTENSION = "_day";
        public const string BOOLEAN_EXTENSION = "_written";

        // administrative additional triples
        public const string LABEL_APPLICATION_PROFILE = "applicationProfile";
        public const string LABEL_BELONGS_TO_PROJECT = "belongsToProject";
        public const string LABEL_GRAPHNAME = "graphName";
        public const string LABEL_IS_PUBLIC = "isPublic";

        //index name
        public const string DEFAULT_ALIAS_NAME = "final_index";

        private readonly RdfClient _RDFClient;

        private readonly IEnumerable<string> _properties;
        private readonly IDictionary<string, string> _labelOfProperties;        
        private readonly IDictionary<string, string> _generalAdditionalTripleLabels = new Dictionary<string, string>()
        {
            { Uris.COSCINE_SEARCH_APPLICATION_PROFILE, LABEL_APPLICATION_PROFILE },
            { Uris.COSCINE_SEARCH_BELONGS_TO_PROJECT, LABEL_BELONGS_TO_PROJECT },
            { Uris.COSCINE_SEARCH_GRAPHNAME, LABEL_GRAPHNAME },
            { Uris.COSCINE_PROJECTSTRUCTURE_IS_PUBLIC_LONG, LABEL_IS_PUBLIC }
        };

        private IDictionary<string, string> _typeOfProperties;

        public ElasticsearchIndexMapper(RdfClient RDFClient, IDictionary<string, string> mapping = null)
        {
            _RDFClient = RDFClient;
            _properties = _RDFClient.GetProperties();
            _labelOfProperties = CreateLabelOfProps(_properties);
            if (mapping == null)
            {
                _typeOfProperties = CreateTypeOfProps(_properties);
            } else
            {
                _typeOfProperties = mapping;
            }     
        }

        /// <summary>
        /// Replaces the current mapping. 
        /// </summary>
        /// <remarks>At first the mappig contains the uniformly generated data types based on the application profiles,
        /// later the types of Elasticsearch.</remarks>
        /// <param name="mapping"></param>
        public void ReplaceMapping(IDictionary<string, string> mapping)
        {
            _typeOfProperties = mapping;

        }

        /// <summary>
        /// Creates the JSON object which contains all information (settings, mappings and aliases) for a new index.
        /// </summary>
        /// <param name="alias">The name of the alias if it should be set directly at the beginning (only in case of initial indexing).</param>
        /// <returns>A JSON object for the request to create a new index.</returns>
        public JObject CreateIndex(string alias = null)
        {
            var jObjectKeyword = new JObject() {
                new JProperty("type", KEYWORD),
            };

            dynamic jObjectText = new JObject();
            jObjectText.type = TEXT;
            jObjectText.fields = new JObject()
            {
                {KEYWORD, jObjectKeyword }
            };

            dynamic jObjectGraphName = new JObject();
            jObjectGraphName.type = KEYWORD;
            jObjectGraphName.doc_values = true;

            var jObjectProperties = new JObject() {
                { LABEL_GRAPHNAME, jObjectGraphName},
                { LABEL_BELONGS_TO_PROJECT, jObjectKeyword},
                { LABEL_APPLICATION_PROFILE, jObjectText},
                { LABEL_IS_PUBLIC, new JObject(){{ "type", BOOLEAN }}}
            };

            foreach (var property in _properties)
            {
                if (String.Equals(property, Uris.RDF_TYPE_LONG))
                {
                    continue;
                }
                var label = GetLabelOfProperty(property);
                if (String.IsNullOrEmpty(label))
                {
                    Console.WriteLine($"Property {property} could not be indexed because no label was found.");
                    continue;
                }
                
                var type = GetTypeOfProperty(property);

                if (String.Equals(type, TEXT))
                {
                    jObjectProperties.Add(label, jObjectText);
                }
                else
                {
                    jObjectProperties.Add(label, new JObject() { { "type", type } });
                }

                // additional fields for types
                if (String.Equals(type, DATE))
                {
                    jObjectProperties.Add(label + MONTH_EXTENSION, jObjectText);
                    jObjectProperties.Add(label + YEAR_EXTENSION, new JObject() { { "type", INTEGER } });
                    jObjectProperties.Add(label + DAY_EXTENSION, new JObject() { { "type", INTEGER } });
                }
                else if (String.Equals(type, BOOLEAN))
                {
                    jObjectProperties.Add(label + BOOLEAN_EXTENSION, jObjectText);
                }
            }

            dynamic finalJObject = new JObject();
            finalJObject.settings = new JObject() as dynamic;
            finalJObject.settings.index = new JObject() as dynamic;
            finalJObject.settings.index.blocks = new JObject() as dynamic;
            finalJObject.settings.index.blocks.read_only_allow_delete = false;
            finalJObject.settings.analysis = new JObject() as dynamic;
            finalJObject.settings.analysis.analyzer = new JObject() as dynamic;
            finalJObject.settings.analysis.analyzer.@default = new JObject() as dynamic;
            finalJObject.settings.analysis.analyzer.@default.type = "english";
            finalJObject.mappings = new JObject() as dynamic;
            finalJObject.mappings.properties = jObjectProperties;

            if (alias != null)
            {
                finalJObject.Add(new JProperty("aliases", new JObject
                    {
                        new JProperty(alias, new JObject())
                    })
                );
            }

            return finalJObject;
        }

        public string GetLabelOfProperty(string property)
        {
            if (!_labelOfProperties.ContainsKey(property))
            {
                // label of specific additional triples
                _labelOfProperties[property] = CreateLabelOfProperty(property);
            }
            return _labelOfProperties[property];
        }

        public string GetTypeOfProperty(string property)
        {
            if (_typeOfProperties.ContainsKey(property))
            {
                return _typeOfProperties[property];
            } 
            else
            {
                return null;
            }
            
        }

        /// <summary>
        /// Creates the labels (field names) for all properties.
        /// </summary>
        /// <param name="properties">An enumerator containing all properties (metadata fields) as URIs.</param>
        /// <returns>A dictionary containing properties (key) and corresponding labels (value).</returns>
        private IDictionary<string, string> CreateLabelOfProps(IEnumerable<string> properties)
        {
            IDictionary<string, string> result = new Dictionary<string, string>();
            foreach (var prop in properties)
            {
                result.Add(prop, CreateLabelOfProperty(prop));
            }

            // add fixed labels (of general additional properties)
            foreach (var prop in _generalAdditionalTripleLabels)
            {
                result.Add(prop.Key, prop.Value);
            }

            return result;
        }

        /// <summary>
        /// Creates the label of a property.
        /// </summary>
        /// <param name="property">The property as URI.</param>
        /// <returns>The created label.</returns>
        private string CreateLabelOfProperty(string property)
        {
            // check for rdfs:label
            var label = _RDFClient.GetRdfsLabel(property);

            if (String.IsNullOrEmpty(label))
            {
                // try to guess the label
                label = _RDFClient.GuessLabel(property);

                if (String.IsNullOrEmpty(label))
                {
                    // use the first shacl name description in profiles 
                    var profileNames = _RDFClient.GetApplicationProfilesNamesOfProperty(property);
                    if (profileNames.Count() != 0)
                    {
                        label = profileNames.First();
                    }
                    else
                    {
                        Console.WriteLine($"No label was found for {property}");
                        return null;
                    }
                }
            }
            return label.ToLower().Replace(" ", "_");
        }

        /// <summary>
        /// Creates the type for all properties.
        /// </summary>
        /// <param name="properties">An enumerator containing all properties (metadata fields) as URIs.</param>
        /// <returns>A dictionary containing properties (key) and corresponding type (value).</returns>
        private IDictionary<string, string> CreateTypeOfProps(IEnumerable<string> properties)
        {
            IDictionary<string, string> result = new Dictionary<string, string>();
            foreach (var prop in properties)
            {
                result.Add(prop, GetSearchType(_RDFClient.GetTypeOfProperty(prop)));
            }
            return result;
        }
       
        /// <summary>
        /// Maps application profile type to corresponding Elasticsearch type.
        /// </summary>
        /// <param name="type">The application profile type.</param>
        /// <returns>An Elasticsearch type.</returns>
        public string GetSearchType(ApplicationProfileType type)
        {
            switch (type)
            {
                case ApplicationProfileType.BOOLEAN:
                    return BOOLEAN;
                case ApplicationProfileType.INTEGER:
                    return INTEGER;
                case ApplicationProfileType.DATE:
                    return DATE;
                case ApplicationProfileType.STRING:
                    return TEXT;
                case ApplicationProfileType.CLASS:
                    return TEXT;
                default:
                    return TEXT;
            }
        }
    }
}