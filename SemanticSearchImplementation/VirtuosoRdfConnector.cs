﻿using System;
using System.Collections.Generic;
using VDS.RDF;
using VDS.RDF.Query;
using VDS.RDF.Storage;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Implements necessary functions for querying and manipulating the RDF-based knowledge graph.
    /// </summary>
    /// <inheritdoc cref="IRdfConnector"/>
    public class VirtuosoRdfConnector : IRdfConnector
    {
        private readonly SparqlParameterizedString _queryString;
        private readonly VirtuosoManager _manager;

        private const string RULESET_QUERY = "DEFINE input:inference 'ruleset' ";

        // necessary namespaces
        private readonly IDictionary<string, Uri> NAMESPACES = new Dictionary<string, Uri>()
        {
            { "dcterms", new Uri(Uris.DCTERMS) },
            { "rdfs", new Uri(Uris.RDFS) },
            { "rdf", new Uri(Uris.RDF) },
            { "sh", new Uri(Uris.SHACL) },
            { "foaf", new Uri(Uris.FOAF) },
            { "coscineprojectstructure", new Uri(Uris.COSCINE_PROJECTSTRUCTURE) },
            { "owl", new Uri(Uris.OWL) },
            { "qudt", new Uri(Uris.QUDT) },
            { "coscinesearch", new Uri(Uris.COSCINE_SEARCH) }            
        };

        public VirtuosoRdfConnector(string virtuosoServer = "localhost", int dbPort = 1111, string db = "db", string dbUser = "dba", string dbPassword = "dba")
        {
            _queryString = new SparqlParameterizedString();
            _manager = new VirtuosoManager(virtuosoServer, dbPort, db, dbUser, dbPassword);

            foreach (var nameSpace in NAMESPACES)
            {
                _queryString.Namespaces.AddNamespace(nameSpace.Key, nameSpace.Value);
            }
        }

        public Graph GetGraph(string graphName)
        {
            var graph = new Graph();
            _manager.LoadGraph(graph, graphName);
            return graph;
        }

        /// <summary>
        /// Builds the final SPARQLL query which contains possible inference rules and namespaces.
        /// </summary>
        /// <param name="query">String representation of a SPARQL query.</param>
        /// <param name="withInference">Flag which indicates if inference rules should be used.</param>
        /// <returns>Final SPARQL query string.</returns>
        private string GetFinalQueryString(string query, bool withInference = true)
        {
            _queryString.CommandText = query;

            if (withInference)
            {
                return RULESET_QUERY + _queryString.ToString();
            }
            else
            {
                return _queryString.ToString();
            }
        }

        public IGraph QueryWithResultGraph(SparqlParameterizedString query)
        {
            return (IGraph)_manager.Query(GetFinalQueryString(query.ToString(), false));
        }

        public SparqlResultSet QueryWithResultSet(string query, bool withInference = true)
        {
            return (SparqlResultSet)_manager.Query(GetFinalQueryString(query, withInference));
        }

        public SparqlResultSet QueryWithResultSet(SparqlParameterizedString query)
        {
            _queryString.CommandText = query.ToString();
            return (SparqlResultSet)_manager.Query(_queryString.ToString());
        }

        public void Update(SparqlParameterizedString query)
        {
            _queryString.CommandText = query.ToString();
            _manager.Update(_queryString.ToString());
        }
    }
}