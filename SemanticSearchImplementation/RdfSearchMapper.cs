﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Contains the main logic to use a document-based search engine for RDF-based metadata records.
    /// </summary>
    public class RdfSearchMapper
    {
        // default search parameter
        private const int SEARCH_SIZE = 50;
        private const int SEARCH_FROM = 0;
        private const bool SEARCH_ADVANCED = false;
        private const string SEARCH_SORTING = "['_score']";

        private readonly ISearchClient _searchClient;
        private readonly RdfClient _RdfClient;

        private ElasticsearchIndexMapper _indexMapper;
        private int _version;

        /// <summary>
        /// Creates an instance of the class <c>RdfSearchMapper</c>.
        /// </summary>
        /// <param name="connector">Instance of an <c>IRdfConnector</c>.</param>
        /// <param name="searchClient">Instance of an <c>ISearchClient</c>.</param>
        /// <param name="language">Abbreviated string representation of the language used.</param>
        public RdfSearchMapper(IRdfConnector connector, ISearchClient searchClient, string language)
        {
            _RdfClient = new RdfClient(connector, language);
            _searchClient = searchClient;

            _version = _RdfClient.GetCurrentIndexVersion();
            _searchClient.ChangeIndex(GetCurrentIndex());
        }

        /************** SEARCH ************/

        /// <summary>
        /// Executes a search query.
        /// </summary>
        /// <param name="query">The search query of the user.</param>
        /// <param name="user">The user who searches.</param>
        /// <param name="advanced">lag to specify simple or advanced search syntax.</param>
        /// <param name="size">Number of results.</param>
        /// <param name="from">Position from which the results should be returned.</param>
        /// <param name="sorting">Sorting of the results <see>
        /// (see https://www.elastic.co/guide/en/elasticsearch/reference/current/sort-search-results.html). </see></param>
        /// <returns>A task that represents the asynchronous search operation.</returns>
        public async Task SearchAsync(string query, string user, bool advanced = SEARCH_ADVANCED, int size = SEARCH_SIZE, int from = SEARCH_FROM, string sorting = SEARCH_SORTING)
        {
            var projects = _RdfClient.GetProjectsOfUser(user);
            var results = await _searchClient.SearchAsync(query, projects, advanced, size, from, sorting);
            foreach (var result in results)
            {
                Console.WriteLine("[{0};    {1}]", result.Value, result.Key);
            }
        }

        /************** ADD or UPDATE ************/

        /// <summary>
        /// Adds or updates a single document.
        /// </summary>
        /// <param name="graphName">ID of metadata graph.</param>
        /// <returns>A task that represents the asynchronous save operation.</returns>
        public async Task AddDocumentAsync(string graphName)
        {
            _indexMapper = new ElasticsearchIndexMapper(_RdfClient, await _searchClient.GetMappingAsync());
            var documents = CreateDocument(graphName, true);
            await _searchClient.AddDocumentAsync(graphName, documents);
        }

        /************** DELETE ************/

        /// <summary>
        /// Deletes a single document.
        /// </summary>
        /// <param name="graphName">ID of metadata graph.</param>
        /// <returns>A task that represents the asynchronous delete operation.</returns>
        public async Task DeleteDocumentAsync(string graphName)
        {
            _indexMapper = new ElasticsearchIndexMapper(_RdfClient, await _searchClient.GetMappingAsync());
            var applicationProfileId = _RdfClient.GetApplicationProfileOfMetadata(graphName);
            SpecificApplicationProfile profile = new SpecificApplicationProfile(_RdfClient, applicationProfileId);

            // Graph may only be deleted from the RDF database after the mapping has been deleted in the search engine!
            // first the graph is only marked as deleted because additional rules have to be executed because they have effects on other graphs and still need the data of the graph to be deleted
            _RdfClient.MarkGraphAsDeleted(graphName);

            // construct additional triples for specific application profile
            var documents = _RdfClient.CreateFieldsFromAdditionalRule(graphName, profile, _indexMapper, true);
            // only the changes of the other documents are considered because the graph will be deleted
            documents.Remove(graphName);

            await _searchClient.DeleteDocumentAsync(graphName, documents);
        }

        /************** INITIAL INDEX ************/

        /// <summary>
        /// Creates a first index with indexes all existing metadata graphs.
        /// </summary>
        /// <returns>A task that represents the asynchronous create operation.</returns>
        public async Task CreateIndexAsync()
        {
            _indexMapper = new ElasticsearchIndexMapper(_RdfClient);
            var content = _indexMapper.CreateIndex(ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME);

            await _searchClient.CreateIndexAsync(content, GetCurrentIndex());

            _indexMapper.ReplaceMapping(await _searchClient.GetMappingAsync());

            // reindex all existing data
            foreach (var listOfIds in _RdfClient.GetAllMetadataIds())
            {
                List<JObject> documents = new List<JObject>();
                foreach (var graph in listOfIds)
                {
                    documents.Add(CreateDocument(graph, false)[graph]);
                }
                await _searchClient.AddDocumentsAsync(documents);
            }
        }

        /************** RE-INDEX ************/

        /// <summary>
        /// Creates a new index (and mapping) and reindexes all existing metadata graphs.
        /// </summary>
        /// <remarks>This operation is necessary if an application profile changes or a new is added.</remarks>
        /// <returns>A task that represents the asynchronous create operation.</returns>
        public async Task ReIndexAsync()
        {
            // create new index
            _indexMapper = new ElasticsearchIndexMapper(_RdfClient);
            var content = _indexMapper.CreateIndex();
            await _searchClient.CreateIndexAsync(content, ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME + "_" + (_version + 1));

            var oldIndex = GetCurrentIndex();
            _version++; // increase version
            _RdfClient.SetCurrentIndexVersion(_version);
            var newIndex = GetCurrentIndex();            
            _searchClient.ChangeIndex(newIndex);

            _indexMapper.ReplaceMapping(await _searchClient.GetMappingAsync());

            // reindex all existing data
            foreach (var listOfIds in _RdfClient.GetAllMetadataIds())
            {
                List<JObject> documents = new List<JObject>();
                foreach (var graph in listOfIds)
                {
                    documents.Add(CreateDocument(graph, false)[graph]);
                }
                await _searchClient.AddDocumentsAsync(documents);
            }

            // change aliases 
            await _searchClient.SwitchAliasAsync(oldIndex, newIndex);

            // delete old index
            await _searchClient.DeleteIndexAsync(oldIndex);
        }

        /// <summary>
        /// Creates the mapping of a metadata graph.
        /// </summary>
        /// <remarks>Additional rules can influence the mapping of existing metadata graphs.</remarks>
        /// <param name="graphName">ID of the metadata graph.</param>
        /// <param name="changeOtherDocs">A dictionary containing the ID of metadata graphs (key)
        /// and corresponding JSON objects (value).</param>
        /// <returns></returns>
        private IDictionary<string, JObject> CreateDocument(string graphName, bool changeOtherDocs)
        {
            var applicationProfileId = _RdfClient.GetApplicationProfileOfMetadata(graphName);
            SpecificApplicationProfile profile = new SpecificApplicationProfile(_RdfClient, applicationProfileId);

            // create properties 
            var jObject = _RdfClient.CreateFields(_RdfClient.GetTriplesFromGraph(graphName), profile, _indexMapper);

            // construct general additional triples
            jObject.Merge(_RdfClient.CreateFields(profile.GetGeneralAdditionalTriples(graphName), profile, _indexMapper));

            // construct additional triples for specific application profile
            var documents = _RdfClient.CreateFieldsFromAdditionalRule(graphName, profile, _indexMapper, changeOtherDocs);

            if (documents.Count() > 0)
            {
                // return all documents
                documents[graphName].Merge(jObject);
                return documents;
            } 
            else
            {
                // no additional rules
                return new Dictionary<string, JObject> { { graphName, jObject } };
            }
        }

        private string GetCurrentIndex()
        {
            return ElasticsearchIndexMapper.DEFAULT_ALIAS_NAME + "_" + _version;
        }
    }
}