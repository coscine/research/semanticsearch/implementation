﻿using System;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Exception which is thrown if a triple of a metadata graph could not be indexed.
    /// </summary>
    [Serializable]
    public class NotIndexableException : Exception
    {

        private readonly string _reason;

        public NotIndexableException(string reason) : base() => _reason = reason;

        public string Reason => _reason;
    }
}