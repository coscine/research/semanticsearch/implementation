﻿using System.Collections.Generic;
using VDS.RDF;

namespace SemanticSearchImplementation
{
    /// <summary>
    /// Contains methods for an application profile.
    /// </summary>
    public abstract class ApplicationProfile
    {
        protected readonly RdfClient _RDFClient;
        protected readonly IDictionary<string, LiteralRule> _generalLiteralRules;
        protected readonly IEnumerable<AdditionalRule> _generalAdditionalRules;

        public ApplicationProfile(RdfClient RDFClient)
        {
            _RDFClient = RDFClient;
            _generalLiteralRules = _RDFClient.ConstructLiteralRules(Uris.COSCINE_LITERAL_RULES);
            _generalAdditionalRules = _RDFClient.ConstructAdditionalRules(Uris.COSCINE_ADDITIONAL_RULES);
        }

        /// <summary>
        /// Executes general additional rules for a given metadata graph.
        /// </summary>
        /// <param name="graphName">ID of metadata graph.</param>
        /// <returns>An enumerator of general additional triples.</returns>
        public IEnumerable<Triple> GetGeneralAdditionalTriples(string graphName)
        {
            List<Triple> triples = new List<Triple>();
            foreach (var generalAdditionalRule in _generalAdditionalRules)
            {
                var constructedTriplesGeneral = generalAdditionalRule.ExecuteRule(graphName);
                triples.AddRange(constructedTriplesGeneral[graphName]);
                ;
            }
            return triples;
        }
    }
}